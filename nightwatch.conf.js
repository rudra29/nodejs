require('dotenv').config();

module.exports = {
    'src_folders': ['test'],

    'webdriver': {
        'start_process': true,
        'server_path': require('chromedriver').path,
        'port': 9516,
        cli_args: ['--port=9516']
    },

    'test_settings': {
        'default': {
            'desiredCapabilities': {
                'browserName': 'chrome',
                'chromeOptions': {
                    'args': ['--headless']
                }
            }
        }
    }
};
