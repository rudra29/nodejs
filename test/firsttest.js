module.exports = {
    'Mediacorp Login page Title Test': function (browser) {
        browser
            .url('http://mediacorpcoredev.prod.acquia-sites.com/')
            .waitForElementVisible('body')
            .assert.titleContains('Log in');
    }
};